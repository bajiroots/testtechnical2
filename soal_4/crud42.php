<!DOCTYPE html>
<html>
<head>
	<title>crud</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
	<a href="menu4.php">Back</a>
<?php 
	include "koneksi.php";
	//MENAMBAH DATA
	$date = date('Y-m-d ', time());
	$time = date('h:i:s',time());
	$date .= $time;
	if (isset($_POST['nametambah'])) {
		$nametambah=$_POST['nametambah'];
		$brandtambah=$_POST['brandtambah']; 
		$colortambah=$_POST['colortambah']; 
		$descriptiontambah=$_POST['descriptiontambah'];
		$createtambah=$_POST['createtambah'];
		$updatetambah=$_POST['updatetambah'];
		$stocktambah=$_POST['stocktambah']; 
		$number=rand(0,999999999);
		$target_dir="image/";
		$target_file=$target_dir . basename($_FILES['img']['name']);
		$uploadOK=1;
		$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

		if (move_uploaded_file($_FILES["img"]["tmp_name"], $target_file)) {

		echo "File ". basename($_FILES['img']['name'])." telah terupload";

		rename("image/".basename($_FILES['img']['name'])."","image/".$number.".".$imageFileType."");

		$newFileName="".$number.".".$imageFileType."";

		$query = mysqli_query($koneksi,"INSERT INTO cars(name,brand_id,image,color,description,create_time,update_time,stock) 
				VALUES('$nametambah','$brandtambah','$newFileName','$colortambah','$descriptiontambah','$date','$date','$stocktambah')");
		} else{
				echo "maaf telah terjadi eror saat mengupload gambar";
				}
		//$sql2="INSERT INTO customer(name,email,address) VALUES('$nametambah','$emailtambah','$addresstambah')";
		//$query2=mysqli_query($koneksi,$sql2);
		//header("Location: crud42.php");
	}
?> 
<?php
	//aksi
	if (isset($_GET['aksi'])) {
		$aksi=$_GET['aksi'];
		if ($aksi=='delete') {
			$idd=$_GET['id'];
			$nama_file=$_GET['nf'];
			$sql3="DELETE FROM cars where id='$idd'";
			$query3=mysqli_query($koneksi,$sql3);
			$file=("image/$nama_file");
			if (file_exists($file)) {
				if (unlink($file)) {
					echo "succes";
				}else{
					echo "fail";
				}
				echo "eror";
			}
			header("Location: crud42.php");
		}elseif ($aksi=='ubah') {
			$idu=$_GET['id'];
			$sql4="SELECT*FROM cars where id='$idu'";
			$query4=mysqli_query($koneksi,$sql4);
			$data4=mysqli_fetch_array($query4);
			echo "<form enctype='multipart/form-data' method='POST'>";

			echo "<label class='mb-2 mr-sm-2'>ID</label>";
			echo "<input type='text' class='form-control mb-2 mr-sm-2' name='idubah' value='".$data4['id']."' readonly>";

			echo "<label class='mb-2 mr-sm-2'>Name</label>";
			echo "<input type='text' class='form-control mb-2 mr-sm-2' name='namaubah' value='".$data4['name']."'>";

			echo "<label class='mb-2 mr-sm-2'>Image</label>";
			echo "<input type='file' class='form-control mb-2 mr-sm-2' name='img2'>";

			echo "<label class='mb-2 mr-sm-2'>Color</label>";
			echo "<input type='text' class='form-control mb-2 mr-sm-2' name='colorubah' value='".$data4['color']."'>";

			echo "<label class='mb-2 mr-sm-2'>Description</label>";
			echo "<input type='text' class='form-control mb-2 mr-sm-2' name='descriptionubah' value='".$data4['description']."'>";
			echo "<input type='hidden' class='form-control mb-2 mr-sm-2' name='timeubah' value='".$date."'>";

			echo "<label class='mb-2 mr-sm-2'>Stock</label>";
			echo "<input type='number' class='form-control mb-2 mr-sm-2' name='stockubah' value='".$data4['stock']."'>";

			echo "<input type='hidden' class='form-control mb-2 mr-sm-2' name='namafileubah' value='".$data4['image']."'>";
			echo "<input type='submit' name='ubahkirim' value='Ubah' class='btn btn-primary mb-2'>";
			echo "</form>";
		}
	}
	if (isset($_POST['idubah'])) {
			$idu2 = $_POST['idubah'];
			$namaubah2 = $_POST['namaubah'];
			$colorubah2 = $_POST['colorubah'];
			$timeubah2 = $_POST['timeubah'];
			$nama_fileubah2 = $_POST['namafileubah'];
			$descriptionubah2 = $_POST['descriptionubah'];
			$stockubah2 = $_POST['stockubah'];

			$number=rand(0,999999999);
			$target_dir="image/";
			$target_file=$target_dir . basename($_FILES['img2']['name']);
			$uploadOK=1;
			$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

			if (move_uploaded_file($_FILES["img2"]["tmp_name"], $target_file)) {

			echo "File ". basename($_FILES['img2']['name'])." telah terupload";

			rename("image/".basename($_FILES['img2']['name'])."","image/".$number.".".$imageFileType."");

			$newFileName="".$number.".".$imageFileType."";

			$sql5 = "UPDATE cars SET name='".$namaubah2."',image='".$newFileName."',color='".$colorubah2."',update_time='".$timeubah2."',description='".$descriptionubah2."',stock='".$stockubah2."' WHERE id='".$idu2."'";
			$query5 = mysqli_query($koneksi,$sql5);
			$file2 = ("image/$nama_fileubah2");
			unlink($file2);
			header("Location: crud42.php");
		}
		}
?>
<form enctype="multipart/form-data" method="POST">
	<label class='mb-2 mr-sm-2'>Tambah Data</label>
	<input type="text" name="nametambah" class='form-control mb-2 mr-sm-2' placeholder="Name">
	<select name="brandtambah" class='form-control mb-2 mr-sm-2'>
		<?php
		$sql6='SELECT*FROM brand';
		$query6=mysqli_query($koneksi,$sql6);
		while ($data6=mysqli_fetch_array($query6)) {
		?>
		<option value="<?= $data6['id'] ?>"><?= $data6['name'] ?></option>
		<?php
		}
		?>
	</select>
	<input type="file" name="img" class='form-control mb-2 mr-sm-2' accept="image/" placeholder="Image">
	<input type="text" name="colortambah" class='form-control mb-2 mr-sm-2' placeholder="Color">
	<input type="text" name="descriptiontambah" class='form-control mb-2 mr-sm-2' placeholder="Description">
	<input type="hidden" name="createtambah" class='form-control mb-2 mr-sm-2' >
	<input type="hidden" name="updatetambah" class='form-control mb-2 mr-sm-2' >
	<input type="text" name="stocktambah" class='form-control mb-2 mr-sm-2' placeholder="Stock">
	<input type="submit" name="kirim" value="Tambah" class='btn btn-primary mb-2'>
</form>
	<?php
	//menampilkan data
			$sql = "SELECT*FROM cars";
			$query = mysqli_query($koneksi,$sql);

	?>
	<h2>Data Mobil</h2>
	<table border="1" class="table">
		<thead class="thead-dark">
		<tr>
			<th>ID</th>
			<th>Name</th>
			<th>Brand</th>
			<th>Image</th>
			<th>Color</th>
			<th>Description</th>
			<th>Create_Time</th>
			<th>Update_Time</th>
			<th>Stock</th>	
			<th>Action</th> 
		</tr>
		</thead>
	<?php
		while ($data=mysqli_fetch_array($query)) {
			
		
	?>
		<tr>
			<td><?=  $data['id']?></td>
			<td><?=  $data['name']?></td>
			<td><?=  $data['brand_id']?></td>
			<td><?=  $data['image']?></td>
			<td><?=  $data['color']?></td>
			<td><?=  $data['description']?></td>
			<td><?=  $data['create_time']?></td>
			<td><?=  $data['update_time']?></td>
			<td><?=  $data['stock']?></td>
			<td><a href="crud42.php?aksi=delete&id=<?= $data['id'] ?>&nf=<?= $data['image'] ?>">Delete</a> |
				<a href="crud42.php?aksi=ubah&id=<?= $data['id'] ?>">Edit</a></td>
		</tr>
	<?php
		}
	?>
	</table>
</div>
</body>
</html>