<!DOCTYPE html>
<html lang="en">
<head>
  <title>Toko Mobil B</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>

<div class="jumbotron text-center" style="margin-bottom:0">
  <h1>Toko Mobil B</h1> 
  <label>Selamat Datang</label>
</div>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
  <a class="navbar-brand" href="#">Menu</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="collapsibleNavbar">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="crud41.php">Crud Customer</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="crud42.php">Crud Cars</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="crud43.php">Crud Brand</a>
      </li>  
    </ul>
  </div>
</nav>

<div class="container" style="margin-top:30px">
    <div class="col-sm-8" id="result">
      <?php
        include 'koneksi.php';
        $sql="SELECT*FROM cars";
        $query=mysqli_query($koneksi,$sql);
        while ($data=mysqli_fetch_array($query)) {
      ?>
      <h2><?= $data['name'] ?></h2>
      <h5>update, <?= $data['update_time'] ?></h5>
      <div><img src="image/<?= $data['image'] ?>"></div>
      <p>Deskripsi:</p>
      <p><?= $data['description'] ?></p>
      <p>stock: <?= $data['stock'] ?></p>
      <form>
        <input type="hidden" name="stk" value="<?= $data['stock'] ?>">
        <input type="hidden" name="id" value="<?= $data['id'] ?>">
        <input type="submit" name="kirim" value="Beli">
      </form>
      <hr>
      <?php
        }
      ?>
      <!--<h2>TITLE HEADING</h2>
      <h5>Title description, Dec 7, 2017</h5>
      <div class="fakeimg">Fake Image</div>
      <p>Some text..</p>
      <p>Sunt in culpa qui officia deserunt mollit anim id est laborum consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.</p>
      <br>
      <h2>TITLE HEADING</h2>
      <h5>Title description, Sep 2, 2017</h5>
      <div class="fakeimg">Fake Image</div>
      <p>Some text..</p>
      <p>Sunt in culpa qui officia deserunt mollit anim id est laborum consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.</p>-->

    </div>
  </div>
</div>
<?php
  if (isset($_GET['kirim'])) {
    $stk=$_GET['stk'];
    $stkbaru=$stk-'1';
    $id=$_GET['id'];
    $sql2="UPDATE cars SET stock='".$stkbaru."' WHERE id='".$id."'";
    $query2=mysqli_query($koneksi,$sql2);
    header("Location: menu4.php");
  }
?>
<div class="jumbotron text-center" style="margin-bottom:0">
  <p>Footer</p>
</div>
</body>
</html>
